library(tidyverse)
library(tidytext)
library(tokenizers)

library(janeaustenr)
library(dplyr)
library(tidytext)
library(stringr)
library(udpipe)

# install.packages("tidyverse")
# install.packages("tidytext")
# install.packages("tokenizers")
# install.packages("udpipe")

originalBooks <- austen_books() %>%
  group_by(book) %>%
  mutate(text = str_to_lower(text),
         book = book,
         chapter = cumsum(str_detect(text, "^chapter [\\divxlc]"))) %>%
  filter(book == "Pride & Prejudice") %>%
  ungroup()

tidyBooks <- originalBooks() %>%
  unnest_tokens(word, text, token = "ngrams", n = 3) %>%
  filter(chapter == "1") %>%
  filter(!is.na(word)) %>%
  select(word) %>%
  group_by(word) %>%
  summarise(n = n()) %>%
  arrange(desc(n))
# my dear you

commaCount <- austen_books() %>%
  group_by(book) %>%
  mutate(text = str_to_lower(text),
         book = book,
         chapter = cumsum(str_detect(text, "^chapter [\\divxlc]"))) %>%
  filter(book == "Sense & Sensibility") %>%
  ungroup() %>%
  filter(chapter == "5" & text != "") %>%
  select(text) %>%
  unnest_tokens(token, text, token = "ptb") %>%
  filter( token == "," ) %>%
  count()

# 50

unincSkippNgramms <- austen_books() %>%
  filter(book == "Pride & Prejudice") %>%
  select(text) %>%
  unnest_tokens(token, text, token = "skip_ngrams", n = 3, k = 1) %>%
  filter(!is.na(token)) %>%
  unique() %>%
  count()

# 422280

# dm <- udpipe_download_model("english")
model <- udpipe_load_model("english-ewt-ud-2.5-191206.udpipe")

adverbsT <- originalBooks %>%
  filter(chapter == 1 & text != "") %>%
  select(text) %>%
  as.character(text) %>%
  udpipe_annotate(object = model)

df <- as_tibble(adverbsT) %>%
  select(-paragraph_id, -sentence, -xpos) %>%
  filter( upos == "ADV") %>%
  select(token) %>%
  filter( !is.na(token) ) %>%
  group_by(token) %>%
  summarise(n = n()) %>%
  arrange(desc(n))

# so